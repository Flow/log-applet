/* main.c
 *
 * Copyright (c) 2014 Florian Schmaus
 *
 * See COPYING for licensing Copyright
 */

#include "logappletcore.h"
#include "config.h"

#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>


#include <panel-applet.h>

static gboolean gnome_log_applet_fill(PanelApplet *applet)
{
	panel_applet_set_flags(
		applet,
		PANEL_APPLET_EXPAND_MAJOR |
		PANEL_APPLET_EXPAND_MINOR |
		PANEL_APPLET_HAS_HANDLE);

	panel_applet_set_background_widget(applet, GTK_WIDGET(applet));

	GtkWidget* label = log_applet_init();

	gtk_container_add(GTK_CONTAINER(applet), label);
	gtk_widget_show_all(GTK_WIDGET(applet));

	return TRUE;
}

static gboolean log_applet_factory(
	PanelApplet *applet, const gchar *iid, gpointer data)
{
	gboolean retval = FALSE;

	if(!strcmp(iid, "LogApplet"))
		retval = gnome_log_applet_fill(applet);

	if(retval == FALSE) {
		printf("Wrong applet!\n");
		exit(-1);
	}

	return retval;
}

PANEL_APPLET_OUT_PROCESS_FACTORY(
	"LogAppletFactory",
	PANEL_TYPE_APPLET,
#ifdef HAVE_GNOME2
	"LogApplet",
#endif
	log_applet_factory,
	NULL);
