/* xfceapplet.c
 *
 * Copyright (c) 2009 Adam Wick
 * Copyright (c) 2011-2012 Alexander Kojevnikov
 * Copyright (c) 2011 Dan Callaghan
 * Copyright (c) 2012 Ari Croock
 * Copyright (c) 2014 Florian Schmaus
 *
 * See COPYING for licensing information
 */

#include "logappletcore.h"
#include "config.h"

#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>

#include <libxfce4panel/xfce-panel-plugin.h>

static void xfce_log_applet_fill(GtkContainer *container)
{
	GtkWidget* label = log_applet_init();

	gtk_container_add(container, label);
}

static void log_applet_construct(XfcePanelPlugin *plugin)
{
	xfce_log_applet_fill(GTK_CONTAINER(plugin));
	xfce_panel_plugin_set_expand(plugin, TRUE);
	gtk_widget_show_all(GTK_WIDGET(plugin));
}

XFCE_PANEL_PLUGIN_REGISTER_EXTERNAL(
	log_applet_construct);
